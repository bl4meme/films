package com.films;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.CustomViewHolder> {
    private ArrayList<Film> data;
    private RecyclerViewClickListener itemListener;

    MyAdapter(ArrayList<Film> data, RecyclerViewClickListener itemListener) {
        this.data = data;
        this.itemListener = itemListener;
    }


    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false);
        return new CustomViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        double score = data.get(position).getScore();
        holder.title.setText(data.get(position).getTitle() + " (" + data.get(position).getYear() + ")");
        holder.score.setText(Double.toString(score));
        holder.director.setText(data.get(position).getDirector());
        holder.number.setText((position + 1) + ".");

        if (score >= 9)
            holder.scoreImage.setImageResource(R.drawable.ic_full_star);
        else if (score >= 5)
            holder.scoreImage.setImageResource(R.drawable.ic_half_star);
        else
            holder.scoreImage.setImageResource(R.drawable.ic_empty_star);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        TextView score;
        TextView director;
        TextView number;
        ImageView scoreImage;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.titleTextView);
            score = itemView.findViewById(R.id.scoreTextView);
            director = itemView.findViewById(R.id.directorTextView);
            number = itemView.findViewById(R.id.numberTextView);
            scoreImage = itemView.findViewById(R.id.scoreImageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getAdapterPosition());
        }
    }

    public interface RecyclerViewClickListener {
        void recyclerViewListClicked(View v, int position);
    }
}
