package com.films;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapter.RecyclerViewClickListener {

    private DBHelper dbHelper;
    private ArrayList<Film> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DBHelper(this);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        getData();
        MyAdapter myAdapter = new MyAdapter(data, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void getData() {
        data = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DBHelper.TABLE_FILMS, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int titleIndex = cursor.getColumnIndex(DBHelper.KEY_TITLE);
            int yearIndex = cursor.getColumnIndex(DBHelper.KEY_YEAR);
            int countryIndex = cursor.getColumnIndex(DBHelper.KEY_COUNTRY);
            int sloganIndex = cursor.getColumnIndex(DBHelper.KEY_SLOGAN);
            int directorIndex = cursor.getColumnIndex(DBHelper.KEY_DIRECTOR);
            int scoreIndex = cursor.getColumnIndex(DBHelper.KEY_SCORE);
            int imagePathIndex = cursor.getColumnIndex(DBHelper.KEY_IMAGE_PATH);

            do {
                Film film = new Film(
                        cursor.getString(titleIndex),
                        cursor.getInt(yearIndex),
                        cursor.getString(countryIndex),
                        cursor.getString(sloganIndex),
                        cursor.getString(directorIndex),
                        cursor.getDouble(scoreIndex),
                        cursor.getInt(imagePathIndex));
                data.add(film);
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        Intent intent = new Intent(this, ShowActivity.class);
        intent.putExtra("Film", data.get(position));
        startActivity(intent);
    }
}
