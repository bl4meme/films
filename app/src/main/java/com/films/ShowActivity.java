package com.films;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ShowActivity extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        ActionBar actionBar = getSupportActionBar();
        Film film = (Film) getIntent().getSerializableExtra("Film");
        TextView title = findViewById(R.id.titleTextView);
        TextView country = findViewById(R.id.countryTextView);
        TextView slogan = findViewById(R.id.sloganTextView);
        TextView director = findViewById(R.id.directorTextView);
        TextView score = findViewById(R.id.scoreTextView);
        ImageView scoreImage = findViewById(R.id.scoreImageView);
        ImageView mainImage = findViewById(R.id.imageView);

        assert film != null;
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(film.getTitle());
        title.setText(film.getTitle() + "(" + film.getYear() + ")");
        country.setText(film.getCountry());
        slogan.setText(film.getSlogan());
        director.setText("Режиссёр: " + film.getDirector());
        double scoreD = film.getScore();
        score.setText(Double.toString(scoreD));

        if (scoreD >= 9)
            scoreImage.setImageResource(R.drawable.ic_full_star);
        else if (scoreD >= 5)
            scoreImage.setImageResource(R.drawable.ic_half_star);
        else
            scoreImage.setImageResource(R.drawable.ic_empty_star);
        mainImage.setImageResource(film.getImagePath());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
