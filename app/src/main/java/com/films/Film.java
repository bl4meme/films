package com.films;

import java.io.Serializable;

class Film implements Serializable {
    private String title;
    private int year;
    private String country;
    private String slogan;
    private String director;
    private double score;
    private int imagePath;

    Film(String title, int year, String country, String slogan, String director, double score, int imagePath) {
        this.title = title;
        this.year = year;
        this.country = country;
        this.slogan = slogan;
        this.director = director;
        this.score = score;
        this.imagePath = imagePath;
    }

    String getTitle() {
        return title;
    }

    int getYear() {
        return year;
    }

    String getCountry() {
        return country;
    }

    String getSlogan() {
        return slogan;
    }

    String getDirector() {
        return director;
    }

    double getScore() {
        return score;
    }

    int getImagePath() {
        return imagePath;
    }
}