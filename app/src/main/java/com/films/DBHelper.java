package com.films;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "filmsDB";
    static final String TABLE_FILMS = "films";

    private static final String KEY_ID = "_id";
    static final String KEY_TITLE = "title";
    static final String KEY_YEAR = "year";
    static final String KEY_COUNTRY = "country";
    static final String KEY_SLOGAN = "slogan";
    static final String KEY_DIRECTOR = "director";
    static final String KEY_SCORE = "score";
    static final String KEY_IMAGE_PATH = "image";

    private SQLiteDatabase currentDB;
    private Boolean isCreating = false;


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        isCreating = true;
        sqLiteDatabase.execSQL("create table " + TABLE_FILMS + "(" + KEY_ID
                + " integer primary key," + KEY_TITLE + " text," + KEY_COUNTRY + " text,"
                + KEY_DIRECTOR + " text," + KEY_SLOGAN + " text," + KEY_IMAGE_PATH + " text,"
                + KEY_YEAR + " integer," + KEY_SCORE + " REAL" + ")");
        currentDB = sqLiteDatabase;
        fillDB();
        isCreating = false;
        currentDB = null;
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        if (isCreating && currentDB != null) {
            return currentDB;
        }
        return super.getWritableDatabase();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_FILMS);
        onCreate(sqLiteDatabase);
    }

    private void fillDB() {
        ArrayList<Film> films = new ArrayList<>();
        films.add(new Film("Побег из Шоушенка", 1994, "США", "Страх - это кандалы. Надежда - это свобода", "Фрэнк Дарабонт", 9.111, R.drawable.escape));
        films.add(new Film("Зеленая миля", 1999, "США", "Пол Эджкомб не верил в чудеса. Пока не столкнулся с одним из них", "Фрэнк Дарабонт", 9.061, R.drawable.greenmile));
        films.add(new Film("Форрест Гамп", 1994, "США", "Мир уже никогда не будет прежним, после того как вы увидите его глазами Форреста Гампа", "Роберт Земекис", 8.913, R.drawable.forestgump));
        films.add(new Film("Список Шиндлера", 1993, "США", "Этот список - жизнь", "Стивен Спилберг", 8.817, R.drawable.schindlerslist));
        films.add(new Film("1 + 1", 2011, "Франция", "Sometimes you have to reach into someone else's world to find out what's missing in your own", "Оливье Накаш", 8.807, R.drawable.intouchables));
        films.add(new Film("Начало", 2010, "США", "Твой разум - место преступления", "Кристофер Нолан", 8.663, R.drawable.inception));
        films.add(new Film("Леон", 1994, "Франция", "Вы не можете остановить того, кого не видно", "Люк Бессон", 8.681, R.drawable.leon));
        films.add(new Film("Король Лев", 1994, "США", "The Circle of Life", "Роджер Аллерс", 8.773, R.drawable.thelionking));
        films.add(new Film("Бойцовский клуб", 1999, "США", "Интриги. Хаос. Мыло.", "Дэвид Финчер", 8.645, R.drawable.fightclub));
        films.add(new Film("Иван Васильевич меняет профессию", 1973, "СССР", "", "Леонид Гайдай", 8.782, R.drawable.ivan));
        films.add(new Film("Жизнь прекрасна", 1997, "Италия", "An unforgettable fable that proves love, family and imagination conquer all", "Роберто Бениньи", 8.625, R.drawable.lifeisgood));
        films.add(new Film("Достучаться до небес", 1997, "Германия", "Быстрый автомобиль, миллион марок в багажнике, и всего одна неделя жить", "Томас Ян", 8.629, R.drawable.knocking));
        films.add(new Film("Крестный отец", 1972, "США", "Настоящая сила не может быть дана, она может быть взята...", "Фрэнсис Форд Коппола", 8.734, R.drawable.thegodfather));
        films.add(new Film("Криминальное чтиво", 1994, "США", "Just because you are a character doesn't mean you have character", "Квентин Тарантино", 8.619, R.drawable.pulpfiction));
        films.add(new Film("Престиж", 1996, "США", "Дружба, ставшая соперничеством. Соперничество, превратившееся во вражду", "Кристофер Нолан", 8.523, R.drawable.theprestige));
        films.add(new Film("Кавказская пленница!", 2014, "Россия", "История повторяется!", "Максим Воронков", 1.110, R.drawable.kvk));
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        for (Film film : films) {
            cv.put(KEY_TITLE, film.getTitle());
            cv.put(KEY_YEAR, film.getYear());
            cv.put(KEY_COUNTRY, film.getCountry());
            cv.put(KEY_SLOGAN, film.getSlogan());
            cv.put(KEY_DIRECTOR, film.getDirector());
            cv.put(KEY_SCORE, film.getScore());
            cv.put(KEY_IMAGE_PATH, film.getImagePath());
            db.insert(TABLE_FILMS, null, cv);
            cv.clear();
        }
    }

}